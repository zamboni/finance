from django.conf.urls import include, url


urlpatterns = [
    url(r'^api/', include('finance.api.urls', namespace='finance_api'))
]
