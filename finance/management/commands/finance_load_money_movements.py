import datetime
import json

from django.core.management.base import BaseCommand, CommandError
from finance.models import Category, CategoryUser, Context, ContextUser, \
    User, MoneyMovement, MoneyMovementUser

catgories_map = {}
contexts_map = {}


def parseMoneyMovement(money_movement):
    global users_map

    usernames = money_movement['users'].keys()
    users = User.objects.filter(username__in=usernames)
    if len(users) != len(money_movement['users']):
        raise Exception("Users don't match (expected - system): \n\t%s\n\t%s" %
            (usernames, [u.username for u in users]))

    category = Category.objects.filter(
        full_name=money_movement['category'],
        users_relation__user__in=users)
    if len(category) == 0:
        raise Exception("Category %s for users %s does not exists" %
            (money_movement['category'], usernames))
    if len(category) > 1:
        raise Exception("Category %s for users %s is not unique" %
            (['%s -%s' % (c.id, c.full_name) for c in category], usernames))

    category = category[0]
    catgories_map[category.id] = category

    money_movement['data']['movement_date'] = \
        datetime.datetime.strptime(money_movement['data']['movement_date'], "%Y-%m-%d").date()

    context = None
    if 'context' in money_movement:
        context = Context.objects.filter(
            name=money_movement['context'],
            users_relation__user__in=users
        )
        if len(context) == 0:
            raise Exception("Context %s for users %s does not exists" %
                (money_movement['context'], usernames))
        if len(context) > 1:
            raise Exception("Context %s for users %s is not unique" %
                (['%s -%s' % (c.id, c.name) for c in context], usernames))
        context = context[0]
        contexts_map[context.id] = context

    mm = MoneyMovement(category=category, context=context, **money_movement['data'])
    mm.save()
    for user in users:
        MoneyMovementUser.objects.create(user=user, money_movement=mm,
            **money_movement['users'][user.username])



class Command(BaseCommand):
    help = 'Add Money Movements from fixtures'

    def add_arguments(self, parser):
        parser.add_argument('money_movements', type=str)

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Load Money Movements'))

        try:
            money_movements = json.load(open(options['money_movements']))
        except Exception as e:
            raise CommandError('File "%s" is not a valid JSON file' %
                options['money_movements'])

        self.stdout.write('\t%s Money Movements to load' % len(money_movements['money_movements']))

        for money_movement in money_movements['money_movements']:
            parseMoneyMovement(money_movement)

        for category in catgories_map.itervalues():
            for category_user in CategoryUser.objects.filter(category=category):
                category_user.calculate_totals()

        for context in contexts_map.itervalues():
            for context_user in ContextUser.objects.filter(context=context):
                context_user.calculate_totals()

        self.stdout.write(self.style.SUCCESS('Successfully parsed Money Movements'))
