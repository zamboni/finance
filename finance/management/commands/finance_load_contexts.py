import datetime
import json

from django.core.management.base import BaseCommand, CommandError
from finance.models import Context, User, ContextUser

users_map = {}


def parseContext(context):
    global users_map

    ctx = Context.objects.filter(name=context['name'])
    if not len(ctx):
        start_date = context['start_date']
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date() if start_date else None
        end_date = context['end_date']
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date() if end_date else None

        ctx = Context(
            name=context['name'],
            start_date=start_date,
            end_date=end_date,
            attributes_ui=context.get('attributes_ui', {})
        )
        ctx.save()
    if len(context['users']):
        ctx_users = [u.username for u in ctx.users.all()]
        for username in context['users']:
            if username not in ctx_users:
                if username in users_map:
                    user = users_map[username]
                else:
                    user, _ = User.objects.get_or_create(username=username)
                    user.set_password('%s2018' % username)
                    user.save()
                    users_map[username] = user
            ContextUser.objects.create(context=ctx, user=user)


class Command(BaseCommand):
    help = 'Consolidate contexts from fixtures (merge strategy)'

    def add_arguments(self, parser):
        parser.add_argument('contexts', type=str)

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Load contexts'))

        try:
            contexts = json.load(open(options['contexts']))
        except Exception as e:
            raise CommandError('File "%s" is not a valid JSON file' %
                options['contexts'])

        self.stdout.write('\t%s main contexts to load' % len(contexts['contexts']))

        for context in contexts['contexts']:
            parseContext(context)

        self.stdout.write(self.style.SUCCESS('Successfully parsed contexts'))