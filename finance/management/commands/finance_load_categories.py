import json

from django.core.management.base import BaseCommand, CommandError
from finance.models import Category, User, CategoryUser

users_map = {}


def parseCategory(category, meta_level=0, parent=None):
    global users_map

    cat = Category.objects.filter(name=category['name'], meta_level=meta_level)
    if parent:
        cat = cat.filter(parent=parent)
    if not len(cat):
        cat = Category(
            name=category['name'],
            average_flow=category.get('average_flow', 'outcome'),
            attributes_ui=category.get('attributes_ui', {})
        )
        if parent:
            cat.parent = parent
        cat.save()
    if len(category['users']):
        cat_users = [u.username for u in cat.users.all()]
        for username in category['users']:
            if username not in cat_users:
                if username in users_map:
                    user = users_map[username]
                else:
                    user, _ = User.objects.get_or_create(username=username)
                    user.set_password('%s2018' % username)
                    user.save()
                    users_map[username] = user
            CategoryUser.objects.create(category=cat, user=user)
    if "children" in category:
        for child in category["children"]:
            parseCategory(child, meta_level + 1, cat)


class Command(BaseCommand):
    help = 'Consolidate categories from fixtures (merge strategy)'

    def add_arguments(self, parser):
        parser.add_argument('categories', type=str)

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Load categories'))

        try:
            categories = json.load(open(options['categories']))
        except Exception as e:
            raise CommandError('File "%s" is not a valid JSON file' %
                options['categories'])

        self.stdout.write('\t%s main categories to load' % len(categories['categories']))

        for category in categories['categories']:
            parseCategory(category)

        self.stdout.write(self.style.SUCCESS('Successfully parsed categories'))