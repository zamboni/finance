from __future__ import unicode_literals
import datetime
from decimal import Decimal

from django.contrib.auth.models import User
from django.db import models

from djmoney.models.fields import MoneyField
from jsonfield import JSONField
from tagulous.models import TagField

from dashboard.models import AuditLogMixin

CATEGORY_FLOW = (
    ('outcome', 'Outcome'),
    ('income', 'Income'),
)

MONEY_MOVEMENTS = (
    ('+', '+'),
    ('-', '-'),
)



class Budget(models.Model):
    effective_from = models.DateField(null=True, blank=True)
    amount = MoneyField(max_digits=15, decimal_places=2, default_currency='EUR')

    class Meta:
        abstract = True

    def get_budget_name(self):
        return '%s - (+%s)' % (self.amount, self.effective_from)

class CategoryUser(models.Model):
    category = models.ForeignKey('Category', related_name='users_relation')
    user = models.ForeignKey(User, related_name='categories_relation')
    totals = JSONField(blank=True)
    total_last_update = models.DateTimeField(null=True, blank=True)
    name = models.CharField(max_length=255, default='', blank=True)

    class Meta:
        unique_together = ('category', 'user')

    def __str__(self):
        if not self.totals:
            self.totals = {}
        if self.name:
            return self.name
        return self.process_name()

    def process_name(self):
        return '%s - %s' % (self.category, self.user)

    def calculate_totals(self):
        print('Calculate totals for category', self.category)
        totals = {
            'total': 0,
            'years': {}
        }
        for money_movement in self.category.money_movements.filter(user=self.user):
            amount = money_movement.amount
            print('user', self.user.username, 'amount', amount, 'original amount', money_movement.amount)
            totals['total'] += amount
            year = '%s' % money_movement.movement_date.year
            if year not in totals['years']:
                totals['years'][year] = 0
            totals['years'][year] += amount
        totals['total'] = round(float(totals['total']), 2)
        for year in totals['years'].keys():
            totals['years'][year] = round(float(totals['years'][year]), 2)
        self.totals = totals
        self.total_last_update = datetime.datetime.now()
        self.save()


class CategoryUserBudget(Budget):
    category_user = models.ForeignKey(CategoryUser, related_name='budgets')
    name = models.CharField(max_length=255, default='', blank=True)

    class Meta:
        ordering = ['category_user', '-effective_from']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = '%s - %s - %s' % (self.category_user.category.name, self.category_user.user.username, self.get_budget_name())
        super(CategoryUserBudget, self).save(*args, **kwargs)


class Category(AuditLogMixin):
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    full_name = models.TextField(default='', blank='')
    users = models.ManyToManyField(User, through='CategoryUser',
            related_name='finance_categories', blank=True)
    attributes_ui = JSONField(blank=True)
    average_flow = models.CharField(max_length=50, default='outcome', choices=CATEGORY_FLOW)
    meta_level = models.IntegerField(default=0, blank=True)

    class Meta:
        ordering = ['full_name']
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.attributes_ui:
            self.attributes_ui = {}
        if not kwargs.pop('skip_full_name', False):
            self.full_name = self._get_full_name()
        if self.parent:
            self.meta_level = self.parent.meta_level + 1

            if self.parent.attributes_ui.get('color') and self.attributes_ui.get('color') is None:
                self.attributes_ui['color'] = self.parent.attributes_ui.get('color')
            if self.parent.attributes_ui.get('graph') and self.attributes_ui.get('graph') is None:
                self.attributes_ui['graph'] = self.parent.attributes_ui.get('graph')
        super(Category, self).save(*args, **kwargs)
        for child in self.children.all():
            child.save()

    def _get_full_name(self):
        if self.parent:
            return '%s - %s' % (self.parent._get_full_name(), self.name)
        return self.name


class ContextUser(models.Model):
    context = models.ForeignKey('Context', related_name='users_relation')
    user = models.ForeignKey(User, related_name='contexts_relation')
    totals = JSONField(blank=True)
    total_last_update = models.DateTimeField(null=True, blank=True)
    name = models.CharField(max_length=255, default='', blank=True)

    class Meta:
        unique_together = ('context', 'user')

    def __str__(self):
        if not self.totals:
            self.totals = {}
        if self.name:
            return self.name
        return self.process_name()

    def process_name(self):
        return '%s - %s' % (self.context, self.user)

    def calculate_totals(self):
        totals = {
            'total': 0,
            'years': {}
        }
        for money_movement in self.context.money_movements.filter(user=self.user):
            totals['total'] += money_movement.amount
            year = '%s' % money_movement.movement_date.year
            if year not in totals['years']:
                totals['years'][year] = 0
            totals['years'][year] += money_movement.amount
        totals['total'] = float(totals['total'])
        for year in totals['years'].keys():
            totals['years'][year] = float(totals['years'][year])
        self.totals = totals
        self.total_last_update = datetime.datetime.now()
        self.save()


class Context(AuditLogMixin):
    name = models.CharField(max_length=100)
    description = models.TextField(default='', blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    tags = TagField(blank=True)
    users = models.ManyToManyField(User, through='ContextUser',
            related_name='contexts', blank=True)
    attributes_ui = JSONField(blank=True)

    def __str__(self):
        return self.name


class Vendor(AuditLogMixin):
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=5, default='', blank=True)

    def __str__(self):
        return self.name


class Account(AuditLogMixin):
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=5)
    primary = models.BooleanField(blank=True, default=True)
    users = models.ManyToManyField(User, through='AccountUser',
            related_name='accounts', blank=True)
    attributes_ui = JSONField(blank=True)

    def __str__(self):
        return self.name


class AccountUser(models.Model):
    account = models.ForeignKey(Account, related_name='users_relation')
    user = models.ForeignKey(User, related_name='accounts_relation')
    percentage = models.FloatField(default=100.0)
    name = models.CharField(max_length=100, default='', blank='')

    def __str__(self):
        return self.name

    def process_name(self):
        return '%s - %s (%spc)' % (self.account.name, self.user.get_full_name(), self.percentage)

    def save(self, *args, **kwargs):
        self.name = self.process_name()

        super(AccountUser, self).save(*args, **kwargs)


class Transaction(AuditLogMixin):
    amount = MoneyField(max_digits=15, decimal_places=2, default_currency='EUR')
    movement = models.CharField(max_length=2, default='-', choices=MONEY_MOVEMENTS)
    movement_date = models.DateField()
    tags = TagField(blank=True)
    context = models.ForeignKey(Context, null=True, blank=True, related_name='transactions')
    name = models.CharField(max_length=255, default='', blank=True)
    account = models.ForeignKey(Account, related_name='transactions')
    description = models.TextField(default='', blank=True)
    vendor = models.ForeignKey(Vendor, on_delete=models.SET_NULL, null=True, blank=True, related_name='transactions')

    def __str__(self):
        if self.name:
            return self.name
        return self.process_name()

    def process_name(self):
        return '%s - %s - %s%s' % \
            (self.movement_date, self.account.short_name, self.movement, self.amount)

    def save(self, *args, **kwargs):
        self.name = self.process_name()

        super(Transaction, self).save(*args, **kwargs)


class MoneyMovement(AuditLogMixin):
    category = models.ForeignKey(Category, related_name='money_movements')
    context = models.ForeignKey(Context, null=True, blank=True, related_name='money_movements')
    amount = MoneyField(max_digits=15, decimal_places=2, default_currency='EUR')
    movement = models.CharField(max_length=2, default='-', choices=MONEY_MOVEMENTS)
    movement_date = models.DateField()
    name = models.CharField(max_length=255, default='', blank=True)
    user = models.ForeignKey(User, null=True, blank=True, related_name='money_movements')
    transaction = models.ForeignKey(Transaction, null=True, blank=True, related_name='money_movements')

    def __str__(self):
        if self.name:
            return self.name
        return self.process_name()

    def process_name(self):
        name = '%s - %s - %s - %s%s' % \
            (self.movement_date, self.user.username, self.category.name, self.movement, self.amount)
        return name

    def save(self, *args, **kwargs):
        self.name = self.process_name()

        super(MoneyMovement, self).save(*args, **kwargs)

    @property
    def multiplier(self):
        return -1 if self.movement == '-' else 1

    @property
    def value(self):
        return self.multiplier * float(self.amount)
