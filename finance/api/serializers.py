from django.contrib.auth.models import User

from rest_framework import serializers

from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)

from .. import models


"""
Utilities
"""

class IdSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        return obj.id

class UserSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'full_name', 'id')

    def get_full_name(self, user):
        return user.get_full_name()


class TagField(serializers.ListField):

    """
    Color objects are serialized into 'rgb(#, #, #)' notation.
    """
    def to_representation(self, obj):
        return map(str, getattr(obj, self.source))

    def to_internal_value(self, data):
        return data


"""
Models
"""

class CategoryUserBudgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.CategoryUserBudget
        fields = ('id', 'effective_from', 'amount')

class CategoryUserSerializer(serializers.ModelSerializer):
    totals = serializers.JSONField()
    budgets = CategoryUserBudgetSerializer(many=True, read_only=True)

    class Meta:
        model = models.CategoryUser
        exclude = set()


class CategorySerializer(serializers.ModelSerializer):
    users_relation = CategoryUserSerializer(many=True, read_only=True)
    attributes_ui = serializers.JSONField()

    class Meta:
        model = models.Category
        fields = ('id', 'name', 'full_name',
                  'users_relation', 'attributes_ui', 'average_flow',
                  'parent', 'meta_level')
        read_only = ('meta_level')

    def get_user_data(self, obj):
        return obj.users_relation[0]

    def to_representation(self, instance):
        ret = super(CategorySerializer, self).to_representation(instance)
        ret['user_data'] = ret['users_relation'][0]
        if not ret['user_data']['totals']:
            ret['user_data']['totals'] = {'total': 0, 'years': {}}
        del ret['users_relation']
        return ret


class ContextUserSerializer(serializers.ModelSerializer):
    totals = serializers.JSONField()

    class Meta:
        model = models.ContextUser
        exclude = set()


class ContextSerializer(TaggitSerializer, serializers.ModelSerializer):
    users_relation = ContextUserSerializer(many=True, read_only=True)
    attributes_ui = serializers.JSONField()
    tags = TagListSerializerField(required=False)

    class Meta:
        model = models.Context
        exclude = set()

    def get_user_data(self, obj):
        return obj.users_relation[0]

    def to_representation(self, instance):
        ret = super(ContextSerializer, self).to_representation(instance)
        ret['user_data'] = ret['users_relation'][0]
        if not ret['user_data']['totals']:
            ret['user_data']['totals'] = {'total': 0, 'years': {}}
        del ret['users_relation']
        return ret


class AccountUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AccountUser
        fields = ('id', 'user', 'percentage')


class AccountSerializer(serializers.ModelSerializer):
    users_relation = AccountUserSerializer(many=True)
    attributes_ui = serializers.JSONField()

    class Meta:
        model = models.Account
        fields = ('name', 'short_name', 'users_relation', 'primary', 'id', 'attributes_ui')

    def create(self, validated_data):
        users_data = validated_data.pop('users_relation')
        account = models.Account.objects.create(**validated_data)
        for user in users_data:
            models.AccountUser.objects.create(
                account=account,
                user=user['user'],
                percentage=user['percentage'],
            )
        return account

    def update(self, instance, validated_data):
        validated_data.pop('users_relation')

        instance.name = validated_data.get('name', instance.name)
        instance.short_name = validated_data.get('short_name', instance.short_name)
        instance.attributes_ui = validated_data.get('attributes_ui', instance.attributes_ui)
        instance.save()

        return instance


class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vendor
        fields = ('id', 'name', 'short_name')


class TransactionSerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField(required=False)
    money_movements = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = models.Transaction
        fields = ['id', 'amount',
            'movement', 'movement_date',
            'tags', 'context',
            'name', 'account',
            'description',
            'money_movements',
            'vendor',
        ]


class TransactionAccountSerializer(serializers.RelatedField):

    def to_representation(self, value):
        return value.account_id

class TransactionVendorSerializer(serializers.RelatedField):

    def to_representation(self, value):
        return value.vendor_id

class MoneyMovementSerializer(TaggitSerializer, serializers.ModelSerializer):
    vendor = TransactionVendorSerializer(source='transaction', read_only=True)
    account = TransactionAccountSerializer(source='transaction', read_only=True)

    class Meta:
        model = models.MoneyMovement
        fields = ['id', 'amount',
            'movement', 'movement_date',
            'category',
            'context',
            'name',
            'user',
            'transaction',
            'vendor',
            'account',
        ]


class TransactionTagsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Transaction.tags.tag_model
        exclude = set()

