import datetime

from django.contrib.auth.models import User

from rest_framework import viewsets, pagination, status
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView


from django.db.models import Prefetch
from .. import models
from . import serializers

"""
Views
"""

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('last_name', 'first_name')
    serializer_class = serializers.UserSerializer
    pagination_class = None


class AccountViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Account.objects.all().order_by('name')
    serializer_class = serializers.AccountSerializer
    pagination_class = None

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class WithUserRelations:

    def get_queryset_for_user(self, queryset, UserRelation):
        user = None
        if self.request.user.id:
            user = self.request.user
        queryset = queryset.filter(users_relation__user=user).prefetch_related(
            Prefetch('users_relation', UserRelation.objects.filter(user=user))
        )
        return queryset

    def create_with_users(self, attribute, UserSerializer, request, *args, **kwargs):
        c_data = request.data
        users_relation = []
        users = []
        if 'users_relation' in c_data:
            users_relation = c_data['users_relation']
            del c_data['users_relation']
        if 'users' in c_data:
            users = c_data['users']
            del c_data['users']
        if len(users_relation) == 0 and len(users) == 0:
            users.append(request.user.id)

        serializer = self.get_serializer(data=c_data)
        serializer.is_valid(raise_exception=True)
        c = serializer.save()

        if users_relation or users:

            def save_relation(relation):
                relation[attribute] = c.id
                if 'totals' not in relation:
                    relation['totals'] = {}
                cu_serializer = UserSerializer(data=relation)
                cu_serializer.is_valid(raise_exception=True)
                cu_serializer.save()

            if users_relation:
                for user_relation in users_relation:
                    save_relation(user_relation)
            elif users:
                for user in users:
                    save_relation({"user": user})

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CategoryViewSet(WithUserRelations, viewsets.ModelViewSet):
    """
    API endpoint that allows Categorys to be viewed or edited.
    """
    queryset = models.Category.objects.all().order_by('full_name')
    serializer_class = serializers.CategorySerializer
    pagination_class = None

    def get_queryset(self):
        queryset = super(CategoryViewSet, self).get_queryset()
        return self.get_queryset_for_user(queryset, models.CategoryUser)

    def create(self, request, *args, **kwargs):
        return self.create_with_users('category', serializers.CategoryUserSerializer,
            request, *args, **kwargs)


class ContextViewSet(WithUserRelations, viewsets.ModelViewSet):
    """
    API endpoint that allows Contexts to be viewed or edited.
    """
    queryset = models.Context.objects.all()
    serializer_class = serializers.ContextSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = super(ContextViewSet, self).get_queryset()
        return self.get_queryset_for_user(queryset, models.ContextUser)

    def create(self, request, *args, **kwargs):
        return self.create_with_users('context', serializers.ContextUserSerializer,
            request, *args, **kwargs)


class VendorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Vendor.objects.all().order_by('name')
    serializer_class = serializers.VendorSerializer
    pagination_class = None


class TransactionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = super(TransactionViewSet, self).get_queryset()
        queryset = queryset.filter(account__users__in=[self.request.user])
        queryset = queryset.select_related(
            'account',
            'context'
        ).prefetch_related(
            'money_movements__category',
            'tags'
        )
        return queryset

    def create(self, request, *args, **kwargs):
        transaction_data = request.data

        category_data = transaction_data.pop('category', None)
        categories_data = transaction_data.pop('categories', [])
        vendor_data = transaction_data.pop('vendor')

        vendor = None
        if vendor_data:
            if isinstance(vendor_data, int):
                vendor = vendor_data
            if isinstance(vendor_data, str):
                vendor_obj, _ = models.Vendor.objects.get_or_create(name=vendor_data)
                vendor = vendor_obj.id
        transaction_data['vendor'] = vendor

        serializer = self.get_serializer(data=transaction_data)
        serializer.is_valid(raise_exception=True)
        transaction = serializer.save()

        categories_to_process = None
        if categories_data and len(categories_data):
            categories_to_process = categories_data
        elif category_data:
            categories_to_process = [{ 'amount': transaction.amount, 'category': category_data }]
        if not categories_to_process:
            raise Exception('No category has been passed as single value or list.')

        for categoryAmount in categories_to_process:
            category = categoryAmount['category']
            amount = categoryAmount['amount']

            for user_relation in transaction.account.users_relation.all():
                account_user = user_relation.user
                account_amount = amount * user_relation.percentage / 100

                mm = models.MoneyMovement(
                    movement=transaction.movement,
                    movement_date=transaction.movement_date,
                    amount=account_amount,
                    context=transaction.context,
                    category=models.Category.objects.get(id=category),
                    user=account_user,
                    transaction=transaction
                )
                mm.save()

        data = serializer.data

        headers = self.get_success_headers(data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)


class MoneyMovementViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Posts to be viewed or edited.
    """
    queryset = models.MoneyMovement.objects.all().order_by('-movement_date')
    serializer_class = serializers.MoneyMovementSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = super(MoneyMovementViewSet, self).get_queryset()
        queryset = queryset.filter(user=self.request.user)
        queryset = queryset.select_related(
            'category',
            'context',
            'user',
            'transaction__account'
        )
        return queryset

    def check_context(self, context, user):
        user_contexts = models.ContextUser.objects.filter(context=context, user=user)
        if len(user_contexts) == 0:
            cu = models.ContextUser(context=context, user=user)
            cu.save()

    def check_category(self, category, user):
        user_categories = models.CategoryUser.objects.filter(category=category, user=user)
        if len(user_categories) == 0:
            cu = models.CategoryUser(category=category, user=user)
            cu.save()
            cu.calculate_totals()
            if category.parent:
                self.check_category(category.parent, user)

    def create(self, request, *args, **kwargs):
        mm_data = request.data

        serializer = self.get_serializer(data=mm_data)
        serializer.is_valid(raise_exception=True)
        mm = serializer.save()
        mm.save()

        data = serializer.data

        headers = self.get_success_headers(data)
        return Response(data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        mm_data = request.data

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=mm_data)
        serializer.is_valid(raise_exception=True)
        mm = serializer.save()

        data = serializer.data

        headers = self.get_success_headers(data)
        return Response(data, status=status.HTTP_200_OK, headers=headers)


class TransactionTagsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Posts to be viewed or edited.
    """
    queryset = models.Transaction.tags.tag_model.objects.all()
    serializer_class = serializers.TransactionTagsSerializer
    pagination_class = None


class CategoryCalculateTotalsView(CreateAPIView):

    def get(self, request, *args, **kwargs):
        res = {
            'success': True,
            'updated_totals': {}
        }

        return Response(res)

    def post(self, request, *args, **kwargs):
        res = {
            'success': True,
            'updated_totals': {}
        }

        update_all = request.data.get('update_all', False)
        categories_to_update = []
        for category_user in models.CategoryUser.objects.filter(user=request.user):
            if category_user.total_last_update and not update_all:
                if models.MoneyMovement.objects.filter(category=category_user.category,
                        last_edit_date__gte=category_user.total_last_update).count():
                    categories_to_update.append(category_user)
            else:
                categories_to_update.append(category_user)

        for category_user in categories_to_update:
            category_user.calculate_totals()
            res['updated_totals'][category_user.category.id] = {
                'totals': category_user.totals,
                'total_last_update': category_user.total_last_update
            }
        return Response(res)


class ContextCalculateTotalsView(CreateAPIView):

    def get(self, request, *args, **kwargs):
        res = {
            'success': True,
            'updated_totals': {}
        }

        return Response(res)

    def post(self, request, *args, **kwargs):
        res = {
            'success': True,
            'updated_totals': {}
        }

        contexts_to_update = []
        for context_user in models.ContextUser.objects.filter(user=request.user):
            if context_user.total_last_update:
                if models.MoneyMovement.objects.filter(context=context_user.context,
                        last_edit_date__gte=context_user.total_last_update).count():
                    contexts_to_update.append(context_user)
            else:
                contexts_to_update.append(context_user)

        for context_user in contexts_to_update:
            context_user.calculate_totals()
            res['updated_totals'][context_user.context.id] = {
                'totals': context_user.totals,
                'total_last_update': context_user.total_last_update
            }
        return Response(res)


class UserStatsView(APIView):

    def get(self, request, *args, **kwargs):
        user = request.user

        res = {
            'totals': {
                'current_month': {'total': 0, 'budget': 0},
                'previous_month': {'total': 0, 'budget': 0},
            },
        }
        today = datetime.date.today()
        current_month = today.replace(day=1)
        previous_month = get_previous_month(current_month)

        for mm in models.MoneyMovement.objects.filter(user=user, movement_date__gte=previous_month):
            if mm.movement_date.month == current_month.month:
                res['totals']['current_month']['total'] += mm.value
            else:
                res['totals']['previous_month']['total'] += mm.value

        user_cubs = models.CategoryUserBudget.objects.filter(category_user__user=user)

        # DISTINCT does not work with SQLite
        parsed_categories = {}
        for cub in user_cubs.filter(effective_from__lte=today).order_by('-effective_from'):
            cat_id = cub.category_user.category.id
            if cat_id not in parsed_categories:
                res['totals']['current_month']['budget'] += float(cub.amount)
                parsed_categories[cat_id] = 1

        parsed_categories = {}
        for cub in user_cubs.filter(effective_from__lt=current_month).order_by('-effective_from'):
            cat_id = cub.category_user.category.id
            if cat_id not in parsed_categories:
                res['totals']['previous_month']['budget'] += float(cub.amount)
                parsed_categories[cat_id] = 1

        return Response(res)


def get_previous_month(date):
    current_day = date.day
    first_day = date.replace(day=1)
    last_day_prev_month = first_day - datetime.timedelta(days=1)
    prev_month_day = last_day_prev_month.replace(day=current_day)
    return prev_month_day
