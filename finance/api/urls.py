from django.conf.urls import url, include

from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'account', views.AccountViewSet)
router.register(r'user', views.UserViewSet)
router.register(r'category', views.CategoryViewSet)
router.register(r'context', views.ContextViewSet)
router.register(r'transaction', views.TransactionViewSet)
router.register(r'transaction-tags', views.TransactionTagsViewSet)
router.register(r'vendor', views.VendorViewSet)
router.register(r'money-movement', views.MoneyMovementViewSet)

urlpatterns = [
    url(r'^category/calculate-totals/$', views.CategoryCalculateTotalsView.as_view(),
        name='category_calculate_totals'),
    url(r'^context/calculate-totals/$', views.ContextCalculateTotalsView.as_view(),
        name='context_calculate_totals'),

    url(r'^user-stats/$', views.UserStatsView.as_view(), name='user_stats'),

    url(r'^', include(router.urls)),

]
