from django.contrib import admin

from . import models

class TransactionAdmin(admin.ModelAdmin):
    list_display = ['id', 'movement_date', 'account', 'amount']

class MoneyMovementAdmin(admin.ModelAdmin):
    list_display = ['id', 'movement_date', 'user', 'amount', 'category']

# Register your models here.
admin.site.register(models.Account)
admin.site.register(models.AccountUser)
admin.site.register(models.CategoryUser)
admin.site.register(models.CategoryUserBudget)
admin.site.register(models.Category)
admin.site.register(models.ContextUser)
admin.site.register(models.Context)
admin.site.register(models.Transaction, TransactionAdmin)
admin.site.register(models.Transaction.tags.tag_model)
admin.site.register(models.MoneyMovement, MoneyMovementAdmin)
admin.site.register(models.Vendor)
